/* Your answer here */
document.getElementById("text1").style.backgroundColor = "pink";

document.getElementsByClassName("text2")[0].firstElementChild.style.color = "red";

var v = document.querySelectorAll(".text3 span");

// vs = v[0];

// vs.style.color = "red";

v[0].style.color = "blue";

function makeDisappear() {

    document.getElementsByClassName("text5")[0].style.visibility = "hidden";

}

function replaceMe() {

    document.getElementsByClassName("text6")[0].textContent = "blah blah blah"

}

function makeBold() {

    var paragraphs = document.getElementsByTagName("p");

    for(var i = 0; i < paragraphs.length; i++) {

        var p = paragraphs[i];

        p.style.fontWeight = "bold";
    }

}